import { HtmlGeneratorService } from './../services/html-generator.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-html-generator',
  templateUrl: './html-generator.component.html',
  styleUrls: ['./html-generator.component.scss']
})
export class HtmlGeneratorComponent implements OnInit {

  @ViewChild('myckeditor') ckeditor: any;
  name = 'ng2-ckeditor';
  ckeConfig: any;
  content: string;
  roro: string;
  templateName: string;
  constructor(private genService: HtmlGeneratorService) {
  }

  ngOnInit() {
    this.ckeConfig = {
      allowedContent: true,
      extraPlugins: 'divarea'
    };
  }

  onChange($event: any): void {
    console.log(this.content);
  }
  saveTemplate(): void {
    this.genService.postTemplate(this.content, this.templateName)
    .subscribe(response => {
      console.log(response);
      this.roro = response.template;
    });
  }

}
