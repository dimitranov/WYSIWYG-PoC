import { HtmlGeneratorService } from './services/html-generator.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxEditorModule } from 'ngx-editor';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CKEditorModule } from 'ng2-ckeditor';
import { HtmlGeneratorComponent } from './html-generator/html-generator.component';

@NgModule({
  declarations: [
    AppComponent,
    HtmlGeneratorComponent
  ],
  imports: [
    BrowserModule,
    NgxEditorModule,
    FormsModule,
    HttpClientModule,
    Angular2FontawesomeModule,
    CKEditorModule
  ],
  providers: [HtmlGeneratorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
