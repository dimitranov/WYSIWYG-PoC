import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

@Injectable()
export class HtmlGeneratorService {
  root = environment.apiUrl;

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  postTemplate(template: string, name: string): Observable<any> {
    const url = `${this.root}/templates`;
    const  obj = {template, name};
    return this.http.post<any>(url, obj).pipe(
      retry(3),
      catchError(this.handleError),
    );
  }

}
